#!/usr/bin/env python3
import serial
import time
import signal
from functools import reduce
import operator
import datetime
import sys
import binascii
from gpiozero import Button
from gpiozero import LED
#from BIT_Functions import vgWriteObject
import RPi.GPIO as GPIO

# Define Classes
class vgCmd:
    ReadObject = 0
    WriteObject = 1
    WriteString = 2
    WriteStringUnicode = 3
    WriteContrast = 4
    ReportObject = 5
    ReportEvent = 6
    ACK = 0x06
    NAK = 0x15
class vgObject:
    DipSwitch = 0
    Knob = 1
    RockerSwitch = 2
    RotarySwitch = 3
    Slider = 4
    TrackBar = 5
    WinButton = 6
    Meter = 7
    CoolGauge = 8
    CustomDigits = 9
    Form = 10
    Guage = 11
    Image = 12
    Keyboard = 13
    LED = 14
    LEDdigits = 15
    Meter = 16
    Strings = 17
    Thermomter = 18
    UserLED = 19
    Video = 20
    StaticText = 21
    Sound = 22
    Timer = 23
class vgScreen:
    LoadingScreen = 0
    TestMenu = 1
    UI_2_1 = 2
    UI_2_2 = 10
    UI_2_3 = 11
    UI_2_4 = 13
    UI_2_5 = 3
    UI_2_6 = 12
    UI_2_7 = 4
    UI_2_8 = 5
    UI_2_9 = 6
    UI_2_10 = 14
    UI_2_11 = 7
    UI_2_12 = 15


# Parameters
Speed = 5
step  = 5
tLysis = 9*60/100/Speed # 9-10 Minutes
tAmplification = 20*60/100/Speed # 20 Minutes
j = 1
LEDPin = 23
ButtonPin = 21
k = 1
display_port = "/dev/ttyAMA0"
LandingScreen = vgScreen.UI_2_1

# Set up
GPIO.setmode(GPIO.BCM)
GPIO.setup(ButtonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) 
GPIO.setup(LEDPin, GPIO.OUT)  
ser = serial.Serial(port=display_port,
    baudrate= 9600,
    timeout=2,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS)
ser.close()
ser.open()


# Define Functions
def vgWriteObject(id, index, value) :
    time.sleep(0.1)
    global ser
    cmd = chr(vgCmd.WriteObject) + chr(id) + chr(index) + chr(int(value/256)) + chr(value%256)
    cks = reduce(operator.xor, (ord(s) for s in cmd), 0)
    cmd = cmd + chr(cks)
    #print(":".join("{:02x}".format(ord(c)) for c in cmd))
    cmd = cmd.encode('utf-8')
    #print(cmd)
    ser.write(cmd) #
    rsp = ser.read(1)
    #print('Response =', rsp)
    #rsp = list(rsp)
    Message = rsp
    #print('Response =', Message)
    return Message
def vgReadScreen():
	time.sleep(0.2)
	global ser
	cmd = chr(0) + chr(10) + chr(0) + chr(10)
	#print(":".join("{:02x}".format(ord(c)) for c in cmd))
	cmd = cmd.encode('utf-8')
	ser.write(cmd)
	rsp = ser.read(6)
	rsp = map(ord,rsp)
	#print('Response =', rsp)
	rsp = list(rsp)
	Message = rsp[4]
	return Message
def ProgressBar(gauge, length, nextScreen): # Bug of going to next screen
	j = 0
	time.sleep(0.2)
	print('Progress Bar Initialized')
	while j != 100:
			j+=step
			if gauge == 0:
				#print('Gauge 0')
				if j == 100:
					()
				elif j == step:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(1) + chr(11)
				elif j == 10:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(0)
				elif j == 20:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(30)
				elif j == 30:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(20)
				elif j == 40:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(34)
				elif j == 50:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(56)
				elif j == 60:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(54)
				elif j == 70:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(76)
				elif j == 80:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(90)
				elif j == 90:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(80)
				elif j == 99:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(100) + chr(110)
				else:
					()
				#print(":".join("{:02x}".format(ord(c)) for c in cmd))
				cmd = cmd.encode('utf-8')
				ser.write(cmd)
				time.sleep(0.1)
				rsp = ser.read(1)
				#print('Response =', rsp)
				#rsp = list(rsp)
				Message = rsp
				#print('Response =', Message)
				time.sleep(length - 0.1)
			elif gauge == 1:
				#print('Gauge 1')
				if j == 100:
					()
				elif j == step:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(1) + chr(11)
				elif j == 10:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(0)
				elif j == 20:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(31)
				elif j == 30:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(21)
				elif j == 40:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(35)
				elif j == 50:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(57)
				elif j == 60:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(55)
				elif j == 70:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(77)
				elif j == 80:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(91)
				elif j == 90:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(81)
				elif j == 99:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(100) + chr(111)
				else:
					()
				#print(":".join("{:02x}".format(ord(c)) for c in cmd))
				cmd = cmd.encode('utf-8')
				ser.write(cmd)
				time.sleep(length-0.1)
				rsp = ser.read(1)
				#print('Response =', rsp)
				#rsp = list(rsp)
				Message = rsp
				#print('Response =', Message)
				time.sleep(length - 0.1)
			elif gauge == 2:
				#print('Gauge 1')
				if j == 100:
					()
				elif j == step:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(1) + chr(11)
				elif j == 10:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(2)
				elif j == 20:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(28)
				elif j == 30:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(22)
				elif j == 40:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(32)
				elif j == 50:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(58)
				elif j == 60:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(52)
				elif j == 70:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(78)
				elif j == 80:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(88)
				elif j == 90:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(82)
				elif j == 99:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(100) + chr(108)
				else:
					()
				#print(":".join("{:02x}".format(ord(c)) for c in cmd))
				cmd = cmd.encode('utf-8')
				ser.write(cmd)
				time.sleep(length-0.1)
				rsp = ser.read(1)
				#print('Response =', rsp)
				#rsp = list(rsp)
				Message = rsp
				#print('Response =', Message)
				time.sleep(length - 0.1)
			elif gauge == 3:
				#print('Gauge 1')
				if j == 100:
					()
				elif j == step:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(1) + chr(11)
				elif j == 10:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(3)
				elif j == 20:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(29)
				elif j == 30:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(23)
				elif j == 40:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(33)
				elif j == 50:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(59)
				elif j == 60:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(53)
				elif j == 70:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(79)
				elif j == 80:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(89)
				elif j == 90:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(83)
				elif j == 99:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(100) + chr(106)
				else:
					()
				#print(":".join("{:02x}".format(ord(c)) for c in cmd))
				cmd = cmd.encode('utf-8')
				ser.write(cmd)
				time.sleep(length-0.1)
				rsp = ser.read(1)
				#print('Response =', rsp)
				#rsp = list(rsp)
				Message = rsp
				#print('Response =', Message)
				time.sleep(length - 0.1)
			elif gauge == 4:
				#print('Gauge 1')
				if j == 100:
					()
				elif j == step:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(1) + chr(11)
				elif j == 10:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(4)
				elif j == 20:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(26)
				elif j == 30:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(16)
				elif j == 40:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(38)
				elif j == 50:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(60)
				elif j == 60:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(50)
				elif j == 70:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(72)
				elif j == 80:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(94)
				elif j == 90:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(84)
				elif j == 99:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(100) + chr(106)
				else:
					()
				#print(":".join("{:02x}".format(ord(c)) for c in cmd))
				cmd = cmd.encode('utf-8')
				ser.write(cmd)
				time.sleep(length-0.1)
				rsp = ser.read(1)
				#print('Response =', rsp)
				#rsp = list(rsp)
				Message = rsp
				#print('Response =', Message)
				time.sleep(length - 0.1)
			elif gauge == 5:
				#print('Gauge 1')
				if j == 100:
					()
				elif j == step:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(1) + chr(11)
				elif j == 10:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(0)
				elif j == 20:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(31)
				elif j == 30:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(21)
				elif j == 40:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(35)
				elif j == 50:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(57)
				elif j == 60:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(55)
				elif j == 70:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(77)
				elif j == 80:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(91)
				elif j == 90:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(j) + chr(81)
				elif j == 99:
					cmd = chr(1) + chr(11) + chr(gauge) + chr(0) + chr(100) + chr(111)
				else:
					()
				#print(":".join("{:02x}".format(ord(c)) for c in cmd))
				cmd = cmd.encode('utf-8')
				ser.write(cmd)
				time.sleep(length-0.1)
				rsp = ser.read(1)
				#print('Response =', rsp)
				#rsp = list(rsp)
				Message = rsp
				#print('Response =', Message)
				time.sleep(length - 0.1)
			print(j, '% Complete')
		
	print('Stage Finished...')
	time.sleep(1)
	print('Moving Forward...')
	form = vgWriteObject(vgObject.Form, nextScreen, 0)
	time.sleep(1)
	return form
def CheckButton(nextScreen, Timer) :
	time.sleep(0.2)
	form = vgReadScreen()
	Video(Timer)
	Count = 1
	while vgReadScreen() == form:
		time.sleep(0.2)
		button_state = GPIO.input(ButtonPin)
		if button_state == False:
			print('Button Pressed...')
			form = vgWriteObject(vgObject.Form, nextScreen, 0)
			print('Moving Forward...')
			time.sleep(0.1)
			break
		else:
			Video(Timer)
def Heater(Temperature, Length): # Not Done
    return
def Optics(state): #  PD not Done
	if state == 1:
		GPIO.output(LEDPin,GPIO.HIGH)
	else:
		GPIO.output(LEDPin,GPIO.LOW)
def Video(TimerIndex): # Done
	time.sleep(0.2)
	global ser
	#print('Initializing Timer...')
	# Autoplay
	cmd = chr(1) + chr(23) + chr(TimerIndex) + chr(0) + chr(1) + chr(23-TimerIndex)
	#print(":".join("{:02x}".format(ord(c)) for c in cmd))
	cmd = cmd.encode('utf-8')
	ser.write(cmd)
	time.sleep(0.1)
	rsp = ser.read(1)
	#print('Response =', rsp)
	rsp = list(rsp)
	Message = rsp
def CheckPort(id, index, value):
	
	k = 1
	while k == 1: 
		print("Check Port")
		global ser
		cmd = chr(vgCmd.WriteObject) + chr(id) + chr(index) + chr(int(value/256)) + chr(value%256)
		cks = reduce(operator.xor, (ord(s) for s in cmd), 0)
		cmd = cmd + chr(cks)
		cmd = cmd.encode('utf-8')
		ser.write(cmd) #
		rsp = ser.read(1)
		if rsp != '\x06' :
			time.sleep(0.5)
		else:
			print("Got through initialization")
			vgWriteObject(vgObject.Form, LandingScreen, 0)
			k = 2
	time.sleep(1)

# Check everything is normal
CheckPort(vgObject.Form, vgScreen.HomeScreen, 0)
GPIO.output(LEDPin,GPIO.HIGH)
time.sleep(.4)
GPIO.output(LEDPin,GPIO.LOW)
print('Lets GO!')
while (1):
	time.sleep(0.2)
	form = vgReadScreen()
	if form == vgScreen.HomeScreen:
		print('Home Screen')
		CheckButton(vgScreen.UISelection, 0)
	elif form == vgScreen.UISelection:
		print('UI Selection')
		CheckButton(vgScreen.UI_3_1,0)
	elif form == vgScreen.UI_1_1:
		print('UI_1_1')
		CheckButton(vgScreen.UI_1_2,0)	
	elif form == vgScreen.UI_1_2:
		print('UI_1_2')
		CheckButton(vgScreen.UI_1_3,0)
	elif form == vgScreen.UI_1_3:
		print('UI_1_3')
		ProgressBar(0, tLysis, vgScreen.UI_1_4)
		time.sleep(1)
	elif form == vgScreen.UI_1_4: 
		print('UI_1_4')
		CheckButton(vgScreen.UI_1_5,0)
	elif form == vgScreen.UI_1_5:
		print('UI_1_5')
		Optics(1)
		ProgressBar(1, tAmplification, vgScreen.UI_1_6)
		time.sleep(1)
		Optics(0)
		time.sleep(1)
		vgWriteObject(vgObject.Form, vgScreen.UI_1_6, 0)
	elif form == vgScreen.UI_1_6:
		print('UI_1_6')
		CheckButton(vgScreen.UISelection,0)
	elif form == vgScreen.UI_2_1:
		print('UI_2_1')
		CheckButton(vgScreen.UI_2_2,0)
	elif form == vgScreen.UI_2_2:
		print('UI_2_2')
		#Video(0)
		CheckButton(vgScreen.UI_2_3,0)
	elif form == vgScreen.UI_2_3:
		print('UI_2_3')
		#Video(1)
		CheckButton(vgScreen.UI_2_4,1)
	elif form == vgScreen.UI_2_4:
		print('UI_2_4')
		ProgressBar(2, tLysis, vgScreen.UI_2_5)
	elif form == vgScreen.UI_2_5:
		print('UI_2_5')
		#Video(2)
		CheckButton(vgScreen.UI_2_6,2)
	elif form == vgScreen.UI_2_6:
		print('UI_2_6')
		ProgressBar(3, tAmplification, vgScreen.UI_2_7)
	elif form == vgScreen.UI_2_7:
		print('UI_2_7')
		CheckButton(vgScreen.UISelection,0)
	elif form == vgScreen.UI_3_1:
		print('UI_3_1')
		CheckButton(vgScreen.UI_3_2,0)
	elif form == vgScreen.UI_3_2:
		print('UI_3_2')
		CheckButton(vgScreen.UI_3_3,0)
	elif form == vgScreen.UI_3_3:
		print('UI_3_3')
		CheckButton(vgScreen.UI_3_4,0)
	elif form == vgScreen.UI_3_4:
		print('UI_3_4')
		CheckButton(vgScreen.UI_3_5,0)
	elif form == vgScreen.UI_3_5:
		print('UI_3_5')
		CheckButton(vgScreen.UI_3_6,0)
	elif form == vgScreen.UI_3_6:
		print('UI_3_6')
		CheckButton(vgScreen.UI_3_7,0)
	elif form == vgScreen.UI_3_7:
		print('UI_3_7')
		CheckButton(vgScreen.UI_3_8,0)
	elif form == vgScreen.UI_3_8:
		print('UI_3_8')
		CheckButton(vgScreen.UISelection,0)
	else :
		if j <= 5:
			print('ERROR')
			j += 1
		else:
			ser.close()
			ser.open()
			time.sleep(3)
			CheckPort(vgObject.Form, vgScreen.HomeScreen, 0)
		
