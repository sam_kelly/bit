## DNA BIT - Heater PID Control System
# Author: Sam Kelly

from gpiozero import Robot, DigitalInputDevice
from time import sleep
import RPi.GPIO as GPIO
#GPIO.setmode(GPIO.BOARD)


## Set Parameters
heater_pin = 12			# control pin for heating system
tc_pin = 17				# thermocouple pin
led_freq = 50
duty_cycle = 50


class Encoder(object):
	def __init__(self, pin):
		self._value = 0
		encoder = DigitalInputDevice(pin)
		encoder.when_activated = self._increment
		encoder.when_deactivated = self._increment
	def reset(self):
		self._value = 0
	def _increment(self):
		self._value += 1        
	@property
	def value(self):
		return self._value

## Set Pins
heater = (heater_pin)	# set this code (probably pwm)
TC = Encoder(tc_pin)

## Initial Values
oTC_error = 0
TC_sum_error = 0
h = 100			# heater "value" (% value, as it is related to duty cycle)

## Define Parameters
target = 45		# target thermocouple value
loop_speed = 1	# how often the system checks/changes values
KP = 2
KD = 1
KI = 0.5
h_max = 1		# max heater value
h_min = 0		# min heating value

## Script
#GPIO.setup(led_pin, GPIO.OUT)
#p = GPIO.PWM(led_pin, led_freq)
#p.start()

## Heating Control Loop
while True:
	TC_error = target - TC.value
	h += (TC_error * KP) + (oTC_error * KD) + (TC_sum_error * KI)
	h = max(min(h_max, h), h_min)			# Clamp Heater Values
	heater.value = h
	print("TC {} Heater {}".format(TC.value, heater.value))
	time.sleep(loop_speed)
	oTC_error = TC_error
	TC_sum_error += TC_error

p.stop()
GPIO.cleanup()
