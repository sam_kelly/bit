## DNA BIT - Heater PID Control System
# Author: Sam Kelly
# Credit: Adafruit-Blinka

import RPi.GPIO as GPIO
#GPIO.setmode(GPIO.BOARD)
#from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import time
import board
import busio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
i2c = busio.I2C(board.SCL, board.SDA) 				# Create the I2C bus
ads = ADS.ADS1015(i2c)								# Create the ADC object using the I2C bus
PD0 = AnalogIn(ads, ADS.P0)							# Create single-ended input on channel 0
PD1 = AnalogIn(ads, ADS.P1)							# Create single-ended input on channel 1
PD2 = AnalogIn(ads, ADS.P2)							# Create single-ended input on channel 2
PD3 = AnalogIn(ads, ADS.P3)							# Create single-ended input on channel 3
PD01 = AnalogIn(ads, ADS.P0, ADS.P1)				# Create differential input between channel 0 and 1

## Set Parameters
# pd
n = 120000			# number of PD value captures
sample_freq = 100	# number of PD samples per second (aperture)
cycles = 10 		# number of iterative cycles
channels = 1 		# number of optical measurement channels
# led
led_pin = 18		# central LED pin (must be PWM)
led_freq = 25		# pulse frequency of LED
duty_cycle = 50		# % of time LED is on 



## Sample Collection
GPIO.setup(led_pin, GPIO.OUT)
pwm = GPIO.PWM(led_pin, led_freq)
pwm.start(0)
print("Successful Setup")
#time = np.linspace()
t0 = time.time()
a = np.empty([n,channels])
t = np.empty([n,channels])
pwm.start(duty_cycle)
for x in range(n):
	a[x] = PD01.value 
	t[x] = time.time() - t0 
	time.sleep(1/(sample_freq))
pwm.stop()
print(time.time())

## Filtering
a2, a1 	= signal.butter(3, 0.05)
zi 		= signal.lfilter_zi(a2, a1)
z, _ 	= signal.lfilter(a2, a1, a, zi=zi*xn[0])
z2, _ 	= signal.lfilter(a2, a1, z, zi=zi*z[0])
y 		= signal.filtfilt(a2, a1, a)


plt.figure
plt.plot(t, a, 'b', alpha=0.75)
plt.plot(t, z, 'r--', t, z2, 'r', t, y, 'k')
plt.legend(('noisy signal', 'lfilter, once', 'lfilter, twice',
            'filtfilt'), loc='best')
plt.grid(True)
plt.show()


## Frequency Analysis
l = len(a) # length of the signal
print(l)
k = np.arange(l)
print(k)
T = l/sample_freq			# 
print(T)
frq = t 					# two sides frequency range
#print(frq)
#frq = frq[range(l//2)] 		# one side frequency range
#print(frq)

Y = np.fft.fft(a)/l 		# fft computing and normalization
#Y = Y[range(l//2)]
#print(Y)


## Create Plot
fig, ax = plt.subplots(2, 1)
ax[0].plot(t,a)
ax[0].set_xlabel('Time')
ax[0].set_ylabel('Amplitude')
ax[1].plot(frq,abs(Y),'r') # plotting the spectrum
ax[1].set_xlabel('Freq (Hz)')
ax[1].set_ylabel('|Y(freq)|')

#plt.plot(t,a)
plt.show()
print(a)
sys.exit("Script Complete!")
