## DNA BIT - Heater PID Control System
# Author: Sam Kelly

from gpiozero import Robot, DigitalInputDevice
from time import sleep
import RPi.GPIO as GPIO
#GPIO.setmode(GPIO.BOARD)
import numpy as np
import matplotlib.pyplot as plt
import time
import board
import busio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
i2c = busio.I2C(board.SCL, board.SDA) 				# Create the I2C bus
ads = ADS.ADS1015(i2c)								# Create the ADC object using the I2C bus
TC = AnalogIn(ads, ADS.P0, ADS.P1)				# Create differential input between channel 0 and 1


## Set Parameters
in_1 = 18			# control pin for heating system
in_2 = 12
pwm_freq = 500
## Script
GPIO.setup(in_1, GPIO.OUT)
GPIO.setup(in_2, GPIO.OUT)

## Initial Values
oTC_error = 0
TC_sum_error = 0
h = 50			# heater "value" (% value, as it is related to duty cycle)

## Define Parameters
target = 2.19		# target thermocouple value (16.4Kohm)
loop_speed = 0.2	# how often the system checks/changes values
KP = 5
KD = 0.001
KI = 0.0005
h_max = 100		# max heater value
h_min = 0		# min heating value
c = 0.0622277536		# ADC Measurement Constant(calculated manually with voltmeter)

## Heating Test
GPIO.output(12, 0) 
GPIO.output(18, 1)         # set GPIO24 to 1/GPIO.HIGH/True  
sleep(0.5)                 # wait half a second  
GPIO.output(18, 0)         # set GPIO24 to 0/GPIO.LOW/False  
sleep(0.5)                 # wait half a second
## Heating Control Loop
heater = GPIO.PWM(in_1, pwm_freq)
heater.start(h)
while True:
	TC_error = target - TC.voltage*c  
	h += (TC_error * KP) + (oTC_error * KD) + (TC_sum_error * KI)
	h = max(min(h_max, h), h_min)						# Clamp Heater Values
	heater.ChangeDutyCycle(h)
	print("TC {} Error {} Heater % {}".format(TC.voltage*c, TC_error, h))
	time.sleep(loop_speed)
	oTC_error = TC_error
	TC_sum_error += TC_error

p.stop()
GPIO.cleanup()
