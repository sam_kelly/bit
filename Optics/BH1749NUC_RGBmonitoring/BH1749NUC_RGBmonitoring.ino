#include <Sparkfun_BH1749NUC_Arduino_Library.h>
#include <Wire.h>

BH1749NUC rgb;

void setup() {
  Serial.begin(115200);

  if (rgb.begin() != BH1749NUC_SUCCESS)
  {
    Serial.println("Error initializing the rgb sensor.");
    while (1) ;
  }
  
  // IR and RGB gain can be set to either BH1749NUC_GAIN_X1 or
  // BH1749NUC_GAIN_X32
  rgb.setIRGain(BH1749NUC_GAIN_X1);
  rgb.setRGBGain(BH1749NUC_GAIN_X1);
  // Measurement mode can be set to either BH1749NUC_MEASUREMENT_MODE_35_MS,
  // BH1749NUC_MEASUREMENT_MODE_120_MS, or BH1749NUC_MEASUREMENT_MODE_240_MS
  // (35ms, 120ms, 240ms between measurements).
  rgb.setMeasurementMode(BH1749NUC_MEASUREMENT_MODE_240_MS);
}

void loop() {
  Serial.println("Red: " + String(rgb.red()));
  Serial.println("Green: " + String(rgb.green()));
  Serial.println("Blue: " + String(rgb.blue()));
  Serial.println("IR: " + String(rgb.ir()));
  Serial.println("Green2: " + String(rgb.green2()));
  Serial.println();
  delay(1000);
}
