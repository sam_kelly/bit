# BH1749NUC Python Library


## Define Classes
class BH1749NUC_Address_t:
	BH1749NUC_ADDRESS_CLOSED = 0x38,
	BH1749NUC_ADDRESS_OPEN = 0x39,
	BH1749NUC_ADDRESS_INVALID = 0xFF
class BH1749NUC_REGISTER_t:
	BH1749NUC_REGISTER_SYSTEM_CONTROL  = 0x40,
	BH1749NUC_REGISTER_MODE_CONTROL1   = 0x41,
	BH1749NUC_REGISTER_MODE_CONTROL2   = 0x42,
	BH1749NUC_REGISTER_RED_DATA_L      = 0x50,
	BH1749NUC_REGISTER_RED_DATA_H      = 0x51,
	BH1749NUC_REGISTER_GREEN_DATA_L    = 0x52,
	BH1749NUC_REGISTER_GREEN_DATA_H    = 0x53,
	BH1749NUC_REGISTER_BLUE_DATA_L     = 0x54,
	BH1749NUC_REGISTER_BLUE_DATA_H     = 0x55,
	BH1749NUC_REGISTER_RESERVED1       = 0x56,
	BH1749NUC_REGISTER_RESERVED2       = 0x57,
	BH1749NUC_REGISTER_IR_DATA_L       = 0x58,
	BH1749NUC_REGISTER_IR_DATA_H       = 0x59,
	BH1749NUC_REGISTER_GREEN2_DATA_L   = 0x5A,
	BH1749NUC_REGISTER_GREEN2_DATA_H   = 0x5B,
	BH1749NUC_REGISTER_INTERRUPT       = 0x60,
	BH1749NUC_REGISTER_PERSISTENCE     = 0x61,
	BH1749NUC_REGISTER_TH_HIGH_L       = 0x62,
	BH1749NUC_REGISTER_TH_HIGH_H       = 0x63,
	BH1749NUC_REGISTER_TH_LOW_L        = 0x64,
	BH1749NUC_REGISTER_TH_LOW_H        = 0x65,
	BH1749NUC_REGISTER_MANUFACTURER_ID = 0x92
class BH1749NUC_gain_t:
	BH1749NUC_GAIN_FORBIDDEN_0, // 0
	BH1749NUC_GAIN_X1,          // 1
	BH1749NUC_GAIN_FORBIDDEN_2, // 2
	BH1749NUC_GAIN_X32,         // 3,
	BH1749NUC_GAIN_INVALID
# Measurement mode settings (update rate):
class BH1749NUC_measurement_mode_t:
	BH1749NUC_MEASUREMENT_MODE_FORBIDDEN_0, // 0
	BH1749NUC_MEASUREMENT_MODE_FORBIDDEN_1, // 1
	BH1749NUC_MEASUREMENT_MODE_120_MS,      // 2
	BH1749NUC_MEASUREMENT_MODE_240_MS,      // 3
	BH1749NUC_MEASUREMENT_MODE_FORBIDDEN_4, // 4
	BH1749NUC_MEASUREMENT_MODE_35_MS,       // 5
	BH1749NUC_MEASUREMENT_MODE_FORBIDDEN_6, // 6
	BH1749NUC_MEASUREMENT_MODE_FORBIDDEN_7, // 7
	BH1749NUC_MEASUREMENT_MODE_INVALID,
# BH1749NUC interrupt sources:
class BH1749NUC_int_source_t:
	BH1749NUC_INT_SOURCE_RED,
	BH1749NUC_INT_SOURCE_GREEN,
	BH1749NUC_INT_SOURCE_BLUE,
	BH1749NUC_INT_SOURCE_FORBIDDEN,
	BH1749NUC_INT_SOURCE_NEW,
	BH1749NUC_INT_SOURCE_INVALID
# BH1749NUC interrupt persistence:
class BH1749NUC_int_persistence_t:
    BH1749NUC_INT_NEW_DATA,
    BH1749NUC_INT_PERSISTENCE_1,
    BH1749NUC_INT_PERSISTENCE_4,
    BH1749NUC_INT_PERSISTENCE_8,
    BH1749NUC_INT_PERSISTENCE_INVALID
class BH1749NUC_measurement_active_t:
    BH1749NUC_MEASUREMENT_ACTIVE_ACTIVE, # 0
    BH1749NUC_MEASUREMENT_ACTIVE_INACTIVE, # 0
    BH1749NUC_MEASUREMENT_ACTIVE_INVALID

# BH1749NUC error codes:
class BH1749NUC_error_t     BH1749NUC_ERROR_PART_ID         = -5,
    BH1749NUC_ERROR_READ            = -4,
    BH1749NUC_ERROR_WRITE           = -3,
    BH1749NUC_ERROR_INVALID_ADDRESS = -2,
    BH1749NUC_ERROR_UNDEFINED       = -1,
    BH1749NUC_ERROR_SUCCESS         = 1

const BH1749NUC_error_t 
BH1749NUC_SUCCESS = BH1749NUC_ERROR_SUCCESS;

// Container for rgb and IR values:
struct rgb_sense{
    uint16_t red;
    uint16_t green;
    uint16_t blue;
    uint16_t ir;
    uint16_t green2;
};

// Red, green, blue, ir, and green2 register:
typedef enum {
    BH1749NUC_RED,
    BH1749NUC_GREEN,
    BH1749NUC_BLUE,
    BH1749NUC_IR,
    BH1749NUC_GREEN2,
    BH1749NUC_INVALID
} BH1749NUC_color_t;
