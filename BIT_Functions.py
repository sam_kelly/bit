import serial
import time
import signal
from functools import reduce
import operator
import datetime
import sys
import binascii
from gpiozero import Button
from gpiozero import LED
import RPi.GPIO as GPIO

# Define Classes
class vgCmd:
    ReadObject = 0
    WriteObject = 1
    WriteString = 2
    WriteStringUnicode = 3
    WriteContrast = 4
    ReportObject = 5
    ReportEvent = 6
    ACK = 0x06
    NAK = 0x15
class vgObject:
    DipSwitch = 0
    Knob = 1
    RockerSwitch = 2
    RotarySwitch = 3
    Slider = 4
    TrackBar = 5
    WinButton = 6
    Meter = 7
    CoolGauge = 8
    CustomDigits = 9
    Form = 10
    Guage = 11
    Image = 12
    Keyboard = 13
    LED = 14
    LEDdigits = 15
    Meter = 16
    Strings = 17
    Thermomter = 18
    UserLED = 19
    Video = 20
    StaticText = 21
    Sound = 22
    Timer = 23
class vgScreen:
    LoadingScreen = 0
    HomeScreen = 24
    UISelection = 1
    # UI 1: Text Only
    UI_1_1 = 23
    UI_1_2 = 5
    UI_1_3 = 6
    UI_1_4 = 7
    UI_1_5 = 8
    UI_1_6 = 9
    # UI 2: Animations
    UI_2_1 = 2
    UI_2_2 = 10
    UI_2_3 = 11
    UI_2_4 = 12
    UI_2_5 = 13
    UI_2_6 = 14
    UI_2_7 = 15
    # UI 3: Videos
    UI_3_1 = 3
    UI_3_2 = 16
    UI_3_3 = 17
    UI_3_4 = 18
    UI_3_5 = 19
    UI_3_6 = 20
    UI_3_7 = 21
    UI_3_8 = 22

ser = serial.Serial(port=display_port,
    baudrate= 9600,
    timeout=2,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS)
def vgWriteObject(id, index, value) :
    time.sleep(0.1)
    global ser
    cmd = chr(vgCmd.WriteObject) + chr(id) + chr(index) + chr(int(value/256)) + chr(value%256)
    cks = reduce(operator.xor, (ord(s) for s in cmd), 0)
    cmd = cmd + chr(cks)
    #print(":".join("{:02x}".format(ord(c)) for c in cmd))
    cmd = cmd.encode('utf-8')
    #print(cmd)
    ser.write(cmd) #
    rsp = ser.read(1)
    #print('Response =', rsp)
    #rsp = list(rsp)
    Message = rsp
    #print('Response =', Message)
    return Message
